// LoopUsage.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

const int N = 10;

void printNumbers(int lastIndex, bool printOddNumbers);

int main()
{
	for (int i = 0; i < N; i = i + 2)
	{
		cout << i << " ";
	}
	cout << "\n";
	
	printNumbers(15, false);
}

void printNumbers(int lastIndex, bool printOddNumbers) {

	int i = 0;

	if (printOddNumbers)
	{
		i = 1;
	}

	for (i; i < lastIndex; i = i + 2)
	{
		cout << i << " ";
	}

}